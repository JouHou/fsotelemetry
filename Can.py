#!/usr/bin/env python3
"""
A quick and dirty Python script to print can frames into stdout. Currently just for testing purposes, in the future
will be used for Formula Student Oulu telemetry project.
"""
import can


def set_bus_up():
    return can.interface.Bus(bustype='socketcan', channel='vcan0', bitrate='500000')


def print_can_msgs():
    try:
        msg = bus.recv(1)
    except can.CanError:
        print("Message not reveived")
    else:
        print(msg)


if __name__ == '__main__':
    bus = set_bus_up()
    while True:
        print_can_msgs()
